# -*- coding: utf-8 -*-

from datetime import timedelta
from openerp import models, fields, api, exceptions


class Course(models.Model):
    _name = 'openacademy.course'

    name = fields.Char(string='Title', required=True)
    description = fields.Text()
    call_me = fields.Text()

    responsible_id = fields.Many2one(
        'res.users', ondelete='set null', string="Responsible", index=True)
    session_ids = fields.One2many(
        'openacademy.session', 'course_id', string='Sessions')

    @api.multi
    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.search_count(
            [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)

        default['name'] = new_name
        return super(Course, self).copy(default)

    _sql_constraints = [
        ('name_description_check',
         'CHECK(name != description)',
         "The title of the course should not be the description"),

        ('name_unique',
         'UNIQUE(name)',
         "The course title must be unique"),
    ]


class Session(models.Model):
    # from pdb import set_trace
    # set_trace()
    _name = 'openacademy.session'

    name = fields.Char(required=True)
    start_date = fields.Date(default=fields.Date.today)
    duration = fields.Float(digits=(6, 2), help="Duration in days")
    seats = fields.Integer(string="Number of seats")
    active = fields.Boolean(default=True)
    color = fields.Integer()

    instructor_id = fields.Many2one('res.partner', string="Instrutor",
                                    domain=['|', ('instructor', '=', True),
                                            ('category_id.name', 'ilike',
                                             "Teacher")])
    course_id = fields.Many2one('openacademy.course',
                                ondelete='cascade', string="Course",
                                required=True)
    attendee_ids = fields.Many2many('res.partner', string="Attendees")

    taken_seats = fields.Float(string='Taken seats', compute='_taken_seats')
    end_date = fields.Date(string="End Date", store=True,
                           compute="_get_end_date", inverse='_set_end_date')

    hours = fields.Float(string="Duration in hours",
                         compute="_get_hours", inverse="_set_hours")

    attendees_count = fields.Integer(
        string="Attendee count", compute='_get_attendees_count', store=True)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('done', 'Done')], defautl='draft')

    @api.multi
    def act_draft(self):
        self.state = 'draft'

    @api.multi
    def act_confirm(self):
        self.state = 'confirm'

    @api.multi
    def act_done(self):
        self.state = 'done'

    @api.depends('seats', 'attendee_ids')
    def _taken_seats(self):
        for r in self:
            if not r.seats:
                r.taken_seats = 0.0
            else:
                r.taken_seats = 100 * len(r.attendee_ids) / r.seats

    @api.onchange('seats', 'attendee_ids')
    def _verify_valid_seats(self):
        if self.seats < 0:
            return {
                'warning': {
                    'title': "错误的椅子数量",
                    'message': "椅子的数量不能为负值",
                }
            }

        if self.seats < len(self.attendee_ids):
            return {
                'warning': {
                    'title': "太多学生了",
                    'message': "增加椅子或者减少学生数量,或者让他们站着!!",
                }
            }

    @api.depends('start_date', 'duration')
    def _get_end_date(self):
        for r in self:
            if not (r.start_date and r.duration):
                r.end_date = r.start_date
                continue                              # 跳出本次循环
            # Add duration to start_date, but: Monday + 5 days = Saturday, so
            # subtract one seconds to get on Friday instead
            start = fields.Datetime.from_string(r.start_date)
            duration = timedelta(days=r.duration, seconds=-1)
            # timedelta 代表两个时间之间的时间差
            r.end_date = start + duration

    def _set_end_date(self):
        for r in self:
            if not (r.start_date and r.end_date):
                continue
            # Compute the difference between dates,
            # but: Friday - Monday = 4 days,
            # so add one day to get 5 days instead
            start_date = fields.Datetime.from_string(r.start_date)
            end_date = fields.Datetime.from_string(r.end_date)
            r.duration = (end_date - start_date).days + 1

    @api.depends('duration')
    def _get_hours(self):
        for r in self:
            r.hours = r.duration * 24

    def _set_hours(self):
        for r in self:
            r.duration = r.hours / 24

    @api.depends('attendee_ids')
    def _get_attendees_count(self):
        for r in self:
            r.attendees_count = len(r.attendee_ids)

    @api.constrains('instructor_id', 'attendee_ids')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.instructor_id and r.instructor_id in r.attendee_ids:
                raise exceptions.ValidationError(
                    "A session's instructor can't be an attendee")

    _sql_constraints = [
        ('check_number_seats',
         'CHECK(seats < 50)',
         "The seats number is too much!"),
    ]
    # @api.constrains('seats')
    # def _check_seats_number(self):
    #     for r in self:
    #         if r.seats > 50:
    #             raise exceptions.ValidationError(
    #                 "The seats number is too much,must < 50 !")
